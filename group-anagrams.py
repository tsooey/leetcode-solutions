class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        d = collections.defaultdict(list)
        for s in strs:
            k = "".join(sorted(s))
            d[k] += [s]
        return list(d.values())
