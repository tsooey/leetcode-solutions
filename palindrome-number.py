class Solution:
    def isPalindrome(self, x: int) -> bool:
        
        def reverse(x: int) -> int:
            res = 0
            while x:
                x, m = divmod(x, 10)
                res *= 10
                res += m
            return res
        
        if x < 0:
            return False
        else:
            return reverse(x) == x
