class Solution:
    def checkRecord(self, s: str) -> bool:
        c = 0
        for i in range(len(s)-2):
            if s[i:i+3] == "LLL":
                return False
            if s[i] == 'A':
                    c += 1
                    if c > 1:
                        return False
        if s == "AA":
            return False
        return True
