class Solution:
    def trailingZeroes(self, n: int) -> int:
        res = 0
        k = 5
        while n // k != 0:
            res += n // k
            k *= 5
        return res
