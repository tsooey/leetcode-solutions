class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        res = ""
        if strs:
            strs = sorted(strs)
            s1, s2 = strs[0], strs[-1]
            i = 0
            while i < len(s1) and i < len(s2):
                if s1[i] == s2[i]:
                    res += s1[i]
                    i += 1
                else:
                    break
        return res 
