class Solution:
    def uniquePaths(self, c: int, r: int) -> int:
        memo = [[1]*c]*r
        for i in range(1, r):
            for j in range(1, c):
                memo[i][j] = memo[i-1][j] + memo[i][j-1]
        return memo[-1][-1]
