class Solution:
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        dictionary = {}
        for i in range(len(nums)):
            key = target - nums[i]
            if key in dictionary:
                return [dictionary[key], i]
            dictionary[nums[i]] = i
