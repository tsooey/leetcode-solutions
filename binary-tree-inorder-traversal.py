# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        res = []
        def traverse(node, res):
            if node:
                traverse(node.left, res)
                res.append(node.val)
                traverse(node.right, res)
        traverse(root, res)
        return res
