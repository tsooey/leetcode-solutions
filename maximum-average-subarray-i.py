class Solution:
    def findMaxAverage(self, nums: List[int], k: int) -> float:
        m = -math.inf
        s = 0
        for i in range(len(nums)):
            s += nums[i]
            if i >= k:
                s -= nums[i-k]
                m = max(m, s)
            if i >= k-1:
                m = max(m, s)
        return m/k
