class Solution:
    def isValid(self, S: str) -> bool:
        S2 = S.replace("abc", "")
        while S != S2:
            S = S2
            S2 = S2.replace("abc", "")
        return S2 == ""
