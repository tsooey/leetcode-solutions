class Solution:
    def lengthOfLIS(self, nums: List[int]) -> int:
        # ans to cache tuples (num, length)
        # longest sequence that can be made ending on num
        ans = []
        # new lo will always be cached with length of 1
        lo = math.inf
        # res keeps track of longest length
        res = 0
        for i in range(len(nums)):
            n = nums[i]
            if n <= lo:
                lo = n
                ans.append((n, 1))
                res = max(res, 1)
            else:
                # l keeps track of local longest
                l = 0
                for j in range(i):
                    m, val = ans[j]
                    if m < n:
                        l = max(l, val+1)
                        res = max(res, l)
                ans.append((n, l))
        return res
            
