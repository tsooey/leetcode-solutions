class Solution:
    def numTrees(self, n: int) -> int:
        self.memo = [None]*(n+1)
        self.memo[0] = 1
        self.memo[1] = 1
        
        def countTree(a, b):
            if self.memo[a] == None:
                c = 0
                for i in range(1,a+1):
                    c += countTree(i-1, a-i)
                self.memo[a] = c
            if self.memo[b] == None:
                c = 0
                for i in range(1,b+1):
                    c += countTree(i-1, b-i)
                self.memo[b] = c
            if self.memo[a] and self.memo[b]:
                return self.memo[a] * self.memo[b]
               
        count = 0
        for i in range(1,n+1):
            count += countTree(i-1, n-i)
        return count
                

