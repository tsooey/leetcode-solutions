class Solution:
    def numberOfBoomerangs(self, points: List[List[int]]) -> int:
        res = 0
        for x1, y1 in points:
            d = collections.defaultdict(int)
            for x2, y2 in points:
                dx = x2-x1
                dy = y2-y1
                l = dx*dx + dy*dy
                d[l] += 1
            for v in d.values():
                res += v*(v-1)
        return res
