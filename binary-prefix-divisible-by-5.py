class Solution:
    def prefixesDivBy5(self, A: List[int]) -> List[bool]:
        res = []
        value = 0
        for bit in A:
            value <<= 1
            value += bit
            value %= 5
            res.append(value == 0)
        return res
