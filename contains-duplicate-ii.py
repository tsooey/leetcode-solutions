class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        d = collections.defaultdict(list)
        for i in range(len(nums)):
            n = nums[i]
            d[n] += [i]
            if len(d[n]) >= 2:
                if d[n][-1] - d[n][-2] <= k:
                    return True
        return False
