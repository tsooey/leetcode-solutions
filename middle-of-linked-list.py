# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def middleNode(self, head: ListNode) -> ListNode:
        c = 0
        counter = head
        while counter:
            c += 1
            counter = counter.next
        res = head
        for i in range(c//2):
            res = res.next
        return res
