class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        lo = math.inf
        m = 0
        for p in prices:
            lo = min(lo, p)
            m = max(m,p-lo)
        return m

