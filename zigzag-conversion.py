class Solution:
    def convert(self, s: str, n: int) -> str:
        if n <= 1:
            return s
        else:
            res = [""] * n
            p = 2 * n - 2
            for i in range(len(s)):
                j = i % p
                if j < n:
                    res[j] += s[i]
                else:
                    res[p-j] += s[i]
            return "".join(res)

