class Solution:
    def myAtoi(self, s: str) -> int:
        res = 0
        s = s.lstrip()
        sign = 1
        i = 0
        while i < len(s):
            if i == 0 and (s[i] == '+' or s[i] == '-'):
                sign = 1 if s[i] == '+' else -1
                i += 1
            if i < len(s):
                if ord('0') <= ord(s[i]) <= ord('9'):
                    res *= 10
                    res += ord(s[i]) - ord('0')
                    i += 1
                else:
                    break
            else:
                break    
        if (-2)**31 <= sign * res <= 2**31 - 1:
            return sign * res
        else:
            return (-2)**31 if sign == -1 else 2**31 - 1
