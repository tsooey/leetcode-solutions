class Solution:
    def longestPalindrome(self, s: str) -> int:
        res = 0
        odd = False
        c = collections.Counter(s)
        for ch in c:
            if c[ch] % 2 == 1:
                res += c[ch] - 1
                odd = True
            else:
                res += c[ch]
        return res + 1*odd
