class Solution:
    def findLHS(self, nums: List[int]) -> int:
        c = collections.Counter(nums)
        res = 0
        for n in c:
            if n+1 in c:
                res = max(res, c[n] + c[n+1])
        return res
