class Solution(object):
    def setZeroes(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: None Do not return anything, modify matrix in-place instead.
        """
        rows = [1]*len(matrix)
        if len(matrix) > 0:
            cols = [1]*len(matrix[0])
        
        # checking 0s in matrix
        for row in range(len(rows)):
            for col in range(len(cols)):
                if matrix[row][col] == 0:
                    rows[row] = 0
                    cols[col] = 0
        
        for i in range(len(rows)):
            if rows[i] == 0:
                for j in range(len(cols)):
                    matrix[i][j] = 0
                    
        for j in range(len(cols)):
            if cols[j] == 0:
                for i in range(len(rows)):
                    matrix[i][j] = 0
        
        return matrix
