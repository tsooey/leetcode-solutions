# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def largestValues(self, root: TreeNode) -> List[int]:
        res = []
        if root:
            dic = collections.defaultdict(list)
            q = collections.deque([(0, root)])
            h = 0
            while q:
                pop = q.popleft()
                key = pop[0]
                node = pop[1]
                if node:
                    dic[key] += [node.val]
                    q.append((key+1, node.left))
                    q.append((key+1, node.right))
                    h = max(h, key+1)
            for i in range(h):
                res.append(max(dic[i]))
        return res
