class Solution:
    def uniquePathsIII(self, grid: List[List[int]]) -> int:
        if grid:
            self.total = 0
            self.rows = len(grid)
            self.cols = (0, len(grid[0]))[len(grid) > 0]
            n = self.rows * self.cols - abs(sum(map(sum,grid))-5)
            
            # create list of unvisited squares
            unvisited = []
            for i in range(self.rows):
                for j in range(self.cols):
                    if grid[i][j] == 0:
                        unvisited.append([i,j])
                    elif grid[i][j] == 1:
                        self.start = [i,j]
                    elif grid[i][j] == 2:
                        self.end = [i,j]
                    
            self.dfs(grid, self.start, unvisited)
            return self.total
        else:
            return 0
        
    def dfs(self, grid, pos, unvisited):
        row = pos[0]
        col = pos[1]
        if unvisited == [] and self.end in [[row,col-1],[row,col+1],[row+1,col],[row-1,col]]:
            self.total += 1
        else:
            for i in range(len(unvisited)):
                square = unvisited[i]
                if [row,col-1] == square:
                    self.dfs(grid, [row,col-1], unvisited[:i]+unvisited[i+1:])
                elif [row,col+1] == square:
                    self.dfs(grid, [row,col+1], unvisited[:i]+unvisited[i+1:])
                elif [row-1,col] == square:
                    self.dfs(grid, [row-1,col], unvisited[:i]+unvisited[i+1:])
                elif [row+1,col] == square:
                    self.dfs(grid, [row+1,col], unvisited[:i]+unvisited[i+1:])
            return
            
            
