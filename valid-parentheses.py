class Solution:
    def isValid(self, s: str) -> bool:
        stk = []
        for ch in s:
            if ch in "({[":
                stk += [ch]
            elif not stk or \
                 not ((stk[-1] == '(' and ch == ')') or \
                      (stk[-1] == '{' and ch == '}') or \
                      (stk[-1] == '[' and ch == ']')):
                return False
            else:
                stk.pop()
        return stk == []
