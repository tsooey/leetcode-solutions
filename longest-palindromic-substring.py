class Solution:
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        # odd palindrome centered around s[i]
        ans = ''
        for i in range(len(s)):
            # mininum amount of letters to expand to on either end
            n = min(i, len(s)-1-i)
            for j in range(n+1):
                # begining index and ending index
                b = i-j
                e = i+j
                if s[b] == s[e]:
                    if e-b+1 > len(ans):
                        ans = s[b:e+1]
                else:
                    break
        # even palindrome, starting from s[i] and s[i+1]
        for i in range(len(s)-1):
            n = min(i, len(s)-1-(i+1))
            for j in range(n+1):
                b = i-j
                e = i+1+j
                if s[b] == s[e]:
                    if e-b+1 > len(ans):
                        ans = s[b:e+1]
                else:
                    break
        return ans
                    
