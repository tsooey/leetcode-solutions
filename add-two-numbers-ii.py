# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        n1 = 0
        while l1 != None:
            n1 = n1 * 10 + l1.val
            l1 = l1.next
        n2 = 0
        while l2 != None:
            n2 = n2 * 10 + l2.val
            l2 = l2.next
        n3 = n1 + n2
        tail = None
        head = ListNode(n3%10)
        while n3 != 0:
            head = ListNode(n3%10)
            head.next = tail
            tail = head
            n3 /= 10
        return head
            