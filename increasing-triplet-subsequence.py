class Solution:
    def increasingTriplet(self, nums: List[int]) -> bool:
        i = j = math.inf
        for n in nums:
            if n < i:
                i = n
            elif i < n < j:
                j = n
            elif n > j:
                return True
        return False
