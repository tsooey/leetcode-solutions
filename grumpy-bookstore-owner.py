class Solution:
    def maxSatisfied(self, customers: List[int], grumpy: List[int], X: int) -> int:
        m = -math.inf
        s = 0
        
        for i in range(len(customers)):
            if grumpy[i] == 0:
                s += customers[i]
                
        for i in range(len(customers)):
            if i < X:
                if grumpy[i] == 1:
                    s += customers[i]
            if i == X-1:
                m = max(s, m)
            if i >= X:
                if grumpy[i-X] == 1:
                    s -= customers[i-X]
                if grumpy[i] == 1:
                    s += customers[i]
                m = max(s, m)
                
        return m
                
