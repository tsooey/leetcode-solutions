class Solution:
    def reverse(self, x: int) -> int:
        sign = 1 if x > 0 else -1
        res = 0
        x = abs(x)
        while x:
            x, m = divmod(x, 10)
            res *= 10
            res += m
        return sign * res * (res < 2**31)
