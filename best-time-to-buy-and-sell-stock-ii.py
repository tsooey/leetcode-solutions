class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        res = 0
        start = 0
        for end in range(1, len(prices)):
            if prices[end-1] > prices[end]:
                res += prices[end-1] - prices[start]
                start = end
            if end == len(prices)-1:
                res += prices[end] - prices[start]
        return res
