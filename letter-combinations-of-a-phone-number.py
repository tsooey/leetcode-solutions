class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        self.alpha = ["","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"]
        
        def dfs(ds, s, res):
            if ds:
                hd, *tl = ds
                n = ord(hd)-ord('0')
                for ch in self.alpha[n]:
                    dfs(tl, s+ch, res)
            else:
                res += [s]
                
        res = []
        if digits:
            dfs(digits, "", res)
        return res
