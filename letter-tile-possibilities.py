class Solution:
    def numTilePossibilities(self, tiles: str) -> int:
        s = set()
        self.backtrack(tiles, "", s)
        return len(s)
    
    def backtrack(self, tiles, e, s):
        if e not in s and len(e) > 0:
            s.add(e)
        if tiles:
            for i in range(len(tiles)):
                self.backtrack(tiles[:i]+tiles[i+1:], e+tiles[i], s)

