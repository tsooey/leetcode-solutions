class Solution:
    def totalNQueens(self, n):
        """
        :type n: int
        :rtype: int
        """
        ans = []
        for i in range(n):
            self.backtrack(n, 1, [i], ans)
        return len(ans)      
    
    def backtrack(self, n, count, sol, ans):
        if count == n:
            convert = ['.' * i + 'Q' + '.' * (n - 1 - i) for i in sol]
            ans.append(convert)
        else:
            index = [i for i in range(n) if i not in sol]
            for i in index:
                # predicate, to check if there is no conflicting queens
                pred = True
                for j in range(len(sol)):
                    if len(sol)-j == abs(i-sol[j]):
                        pred = False
                        break;
                if pred == True:
                    self.backtrack(n, count+1, sol+[i], ans)