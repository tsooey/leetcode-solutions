class Solution:
    def validSquare(self, p1: List[int], p2: List[int], p3: List[int], p4: List[int]) -> bool:
        
        def l2(p1, p2):
            x1, y1 = p1
            x2, y2 = p2
            dx = x2 - x1
            dy = y2 - y1
            return dx*dx + dy*dy
        
        dists = [l2(p1,p2), l2(p1,p3), l2(p1,p4), l2(p2,p3), l2(p2,p4), l2(p3,p4)]
        d = collections.defaultdict(int)
        for dist in dists:
            d[dist] += 1
        
        return len(d.keys()) == 2 and \
               (list(d.values()) == [2,4] or list(d.values()) == [4,2])
