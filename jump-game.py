class Solution:
    def canJump(self, nums: List[int]) -> bool:
        n = len(nums)-1
        m = 0
        i = 0
        while i <= m:
            m = max(m, nums[i]+i)
            i += 1
            if m >= n:
                return True
        return False
