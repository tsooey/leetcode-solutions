# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def sumNumbers(self, root: TreeNode) -> int:
        
        def dfs(node, num):
            if node:
                num *= 10
                num += node.val
                if node.left != None and node.right != None:
                    return dfs(node.left, num) + dfs(node.right, num)
                elif node.left != None:
                    return dfs(node.left, num)
                elif node.right != None:
                    return dfs(node.right, num)
                else:
                    return num
            else:
                return num
            
        return dfs(root, 0)
