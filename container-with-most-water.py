class Solution:
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        max_area = 0
        i = 0
        j = len(height)-1
        while i < j:
            if height[j] < height[i]:
                max_area = max(max_area, (j-i)*height[j])
                j-=1
            else:
                max_area = max(max_area, (j-i)*height[i])
                i+=1
        return max_area
