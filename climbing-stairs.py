class Solution:
    def climbStairs(self, n: int) -> int:
        if n <= 2:
            return n
        else:
            i = 1
            j = 2
            while n > 2:
                i, j = j, i + j
                n -= 1
            return j
